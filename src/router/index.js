import { createRouter, createWebHistory } from "vue-router";
import jwt_decode from "jwt-decode";
import Home from "../views/Home.vue";
import Forbidden from "../views/Error/503.vue";
import NotFound from "../views/Error/404.vue";

import Login from "../views/Auth/Login.vue";
import Register from "../views/Auth/Register.vue";
import Social from "../views/Auth/Social.vue";

// CUSTOMER
import DashboardCustomer from "../views/Dashboard/Customer/Index.vue";
import DashboardCustomerPacket from "../views/Dashboard/Customer/Packet/Index.vue";
import DashboardCustomerPacketSchedule from "../views/Dashboard/Customer/Packet/Schedule.vue";
import DashboardCustomerPacketBill from "../views/Dashboard/Customer/Packet/Bill.vue";
import DashboardCustomerPacketBillDetail from "../views/Dashboard/Customer/Packet/BillDetail.vue";

// TECH
import DashboardTech from "../views/Dashboard/Tech/Index.vue";
import DashboardTechPacket from "../views/Dashboard/Tech/Packet/Index.vue";
import DashboardTechCustomer from "../views/Dashboard/Tech/Customer/Index.vue";
import DashboardTechCustomerSchedule from "../views/Dashboard/Tech/Customer/Schedule.vue";

// MARKETING
import DashboardMarketing from "../views/Dashboard/Marketing/Index.vue";
import DashboardMarketingCustomer from "../views/Dashboard/Marketing/Customer/Index.vue";
import DashboardMarketingCustomerPacket from "../views/Dashboard/Marketing/Customer/Packet.vue";

// FINANCE
import DashboardFinance from "../views/Dashboard/Finance/Index.vue";
import DashboardFinanceSchedule from "../views/Dashboard/Finance/Schedule/Index.vue";
import DashboardFinanceBill from "../views/Dashboard/Finance/Bill/Index.vue";
import DashboardFinanceBillDetail from "../views/Dashboard/Finance/Bill/Detail.vue";
import DashboardFinanceBillPdfExport from "../views/Dashboard/Finance/Bill/PdfExport.vue";
import DashboardFinanceCustomer from "../views/Dashboard/Finance/Customer/Index.vue";
import DashboardFinanceCustomerSchedule from "../views/Dashboard/Finance/Customer/Schedule.vue";
import DashboardFinanceCustomerBill from "../views/Dashboard/Finance/Customer/Bill.vue";
import DashboardFinanceCustomerBillDetail from "../views/Dashboard/Finance/Customer/BillDetail.vue";

import DashboardProfile from "../views/Dashboard/Profile.vue";

const routes = [
  {
    path: '/not-found',
    component: NotFound
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: "/not-found",
  },
  {
    path: "/forbidden",
    name: "Forbidden",
    component: Forbidden,
  },
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      requiredWithoutAuth: true,
    },
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
    meta: {
      requiredWithoutAuth: true,
    },
  },
  {
    path: "/social/:provider/callback",
    name: "Social",
    component: Social,
    meta: {
      requiredWithoutAuth: true,
    },
  },

  // CUSTOMER
  {
    path: "/dashboard/customer",
    name: "Dashboard Customer",
    component: DashboardCustomer,
    meta: {
      requiresAuth: true,
      role: ["C"],
    },
  },
  {
    path: "/dashboard/customer/packet",
    name: "Dashboard Customer Packet",
    component: DashboardCustomerPacket,
    meta: {
      requiresAuth: true,
      role: ["C"],
    },
  },
  {
    path: "/dashboard/customer/packet/:id/schedule",
    name: "Dashboard Customer Packet Schedule",
    component: DashboardCustomerPacketSchedule,
    meta: {
      requiresAuth: true,
      role: ["C"],
    },
  },
  {
    path: "/dashboard/customer/packet/:id/bill",
    name: "Dashboard Customer Packet Bill",
    component: DashboardCustomerPacketBill,
    meta: {
      requiresAuth: true,
      role: ["C"],
    },
  },
  {
    path: "/dashboard/customer/packet/:id/bill/:billId",
    name: "Dashboard Customer Packet Bill Detail",
    component: DashboardCustomerPacketBillDetail,
    meta: {
      requiresAuth: true,
      role: ["C"],
    },
  },

  // TECH
  {
    path: "/dashboard/tech",
    name: "Dashboard Tech",
    component: DashboardTech,
    meta: {
      requiresAuth: true,
      role: ["T"],
    },
  },
  {
    path: "/dashboard/tech/packet",
    name: "Dashboard Tech Packet",
    component: DashboardTechPacket,
    meta: {
      requiresAuth: true,
      role: ["T"],
    },
  },
  {
    path: "/dashboard/tech/customer",
    name: "Dashboard Tech Customer",
    component: DashboardTechCustomer,
    meta: {
      requiresAuth: true,
      role: ["T"],
    },
  },
  {
    path: "/dashboard/tech/customer/:id/schedule",
    name: "Dashboard Tech Customer Schedule",
    component: DashboardTechCustomerSchedule,
    meta: {
      requiresAuth: true,
      role: ["T"],
    },
  },

  // MARKETING
  {
    path: "/dashboard/marketing",
    name: "Dashboard Marketing",
    component: DashboardMarketing,
    meta: {
      requiresAuth: true,
      role: ["M"],
    },
  },
  {
    path: "/dashboard/marketing/customer",
    name: "Dashboard Marketing Customer",
    component: DashboardMarketingCustomer,
    meta: {
      requiresAuth: true,
      role: ["M"],
    },
  },
  {
    path: "/dashboard/marketing/customer/:id/packet",
    name: "Dashboard Marketing Customer Packet",
    component: DashboardMarketingCustomerPacket,
    meta: {
      requiresAuth: true,
      role: ["M"],
    },
  },

  // FINANCE
  {
    path: "/dashboard/finance",
    name: "Dashboard Finance",
    component: DashboardFinance,
    meta: {
      requiresAuth: true,
      role: ["F"],
    },
  },
  {
    path: "/dashboard/finance/schedule",
    name: "Dashboard Finance Schedule",
    component: DashboardFinanceSchedule,
    meta: {
      requiresAuth: true,
      role: ["F"],
    },
  },
  {
    path: "/dashboard/finance/bill",
    name: "Dashboard Finance Bill",
    component: DashboardFinanceBill,
    meta: {
      requiresAuth: true,
      role: ["F"],
    },
  },
  {
    path: "/dashboard/finance/bill/:id",
    name: "Dashboard Finance Bill Detail",
    component: DashboardFinanceBillDetail,
    meta: {
      requiresAuth: true,
      role: ["F"],
    },
  },
  {
    path: "/dashboard/finance/customer",
    name: "Dashboard Finance Customer",
    component: DashboardFinanceCustomer,
    meta: {
      requiresAuth: true,
      role: ["F"],
    },
  },
  {
    path: "/dashboard/finance/customer/:id/schedule",
    name: "Dashboard Finance Customer Schedule",
    component: DashboardFinanceCustomerSchedule,
    meta: {
      requiresAuth: true,
      role: ["F"],
    },
  },
  {
    path: "/dashboard/finance/customer/:id/bill",
    name: "Dashboard Finance Customer Bill",
    component: DashboardFinanceCustomerBill,
    meta: {
      requiresAuth: true,
      role: ["F"],
    },
  },
  {
    path: "/dashboard/finance/customer/:id/bill/detail/:billId",
    name: "Dashboard Finance Customer Bill Detail",
    component: DashboardFinanceCustomerBillDetail,
    meta: {
      requiresAuth: true,
      role: ["F"],
    },
  },
  {
    path: "/dashboard/finance/bill/pdf-export",
    name: "Dashboard Finance Bill Pdf Export",
    component: DashboardFinanceBillPdfExport,
    meta: {
      requiresAuth: true,
      role: ["F"],
    },
  },

  // ALL
  {
    path: "/dashboard/profile",
    name: "Dashboard Profile",
    component: DashboardProfile,
    meta: {
      requiresAuth: true,
      role: ["C", "T", "M", "F"],
    },
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  routes,
});

router.beforeEach((to, from, next) => {
  // mengecek ada tidak meta requiredWithoutAuth di dalam meta
  if (to.matched.some(record => record.meta.requiredWithoutAuth)) {
    if (localStorage.getItem('token_hmn') != null) {
      window.location.href = "/";
    }
  }

  // mengecek ada tidak meta requiresAuth di dalam meta
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // cek di localstorage token, jika false maka diarahkan ke halaman login
    if (localStorage.getItem('token_hmn') == null) {
      window.location.href = "/login";
    } else {
      // cek permission
      const decodeToken = jwt_decode(localStorage.getItem('token_hmn'));
      if (to.matched.some(record => record.meta.role)) {
        if (!to.meta.role.includes(decodeToken.user.role)) {
          window.location.href = "/forbidden";
        }
      }
    }
  }
  next();
});

export default router;

import { createStore } from "vuex";

export default createStore({
  state: {
    baseUrl: "https://behmn.sudutkampus.com/api/v1/",
    // baseUrl: "http://localhost:8000/api/v1/",
    apiKeyHereMap: "xGhAu0ZqB9ki71zcvPTkna6KvaCdkpdfDKihKDjoiQI",
    dataProfile: null,
    tokenJwt: null,
  },
  mutations: {
    setProfile(state, payload) {
      state.dataProfile = payload;
    },
    setToken(state, payload) {
      state.tokenJwt = payload;
    },
    logout(state) {
      state.dataProfile = null;
      state.tokenJwt = null;
      localStorage.removeItem("token_hmn");
    },
  },
  actions: {},
  modules: {},
});

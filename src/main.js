import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import JQuery from 'jquery'
global.jQuery = JQuery
global.$ = JQuery
import "dropify";
import VueSweetalert2 from 'vue-sweetalert2';
import VueApexCharts from "vue3-apexcharts";
import 'sweetalert2/dist/sweetalert2.min.css';
import TablePagination from "./components/TablePagination.vue";
import LoadingButton from "./components/LoadingButton.vue";
import CardNoDataDashboard from "./components/Dashboard/CardNoData.vue";
import CKEditor from '@ckeditor/ckeditor5-vue';
import VCalendar from 'v-calendar';

// Home
import Navbar from "./components/Navbar.vue";
import Footer from "./components/Footer.vue";

// Auth
import NavbarAuth from "./components/Auth/Navbar.vue";
import FooterAuth from "./components/Auth/Footer.vue";

// Dashboard
import NavbarDashboard from "./components/Dashboard/Navbar.vue";
import FooterDashboard from "./components/Dashboard/Footer.vue";
import SidebarDashboard from "./components/Dashboard/Sidebar.vue";

const app = createApp(App);

// Home
app.component("Navbar", Navbar);
app.component("Footer", Footer);

// Auth
app.component("NavbarAuth", NavbarAuth);
app.component("FooterAuth", FooterAuth);

// Dashboard
app.component("NavbarDashboard", NavbarDashboard);
app.component("FooterDashboard", FooterDashboard);
app.component("SidebarDashboard", SidebarDashboard);
app.component("TablePagination", TablePagination);
app.component("LoadingButton", LoadingButton);
app.component("CardNoDataDashboard", CardNoDataDashboard);

app.use(store);
app.use(router);
app.use(CKEditor);
app.use(VueApexCharts);

app.use(VueSweetalert2);
app.use(VCalendar);

app.mount("#app");
